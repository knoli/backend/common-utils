from uuid import UUID

from pydantic import BaseModel


class OrderInitial(BaseModel):
    poster_id: UUID
    contractor_id: UUID
    campaign_id: UUID
    price: int
    type: str


class OrderId(BaseModel):
    order_id: UUID


class OrderChanges(BaseModel):
    state: str | None = None
    price: int | None = None
    description: str | None = None


class Order(OrderId, OrderInitial):
    chat_id: UUID
    state: str
    description: str
