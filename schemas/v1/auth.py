from pydantic import BaseModel


class AdminData(BaseModel):
    is_admin: bool


class AccountOperationOk(BaseModel):
    account_id: str
