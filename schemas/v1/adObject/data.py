from uuid import UUID

from pydantic import BaseModel


class AdObject(BaseModel):
    title: str
    description: str
    categories: list[int]
    rating: float
    created_time: str
    updated_time: str | None = None


class Owner(AdObject):
    pass


class Poster(AdObject):
    source_links: list[str]
    price: int


class Campaign(Owner):
    campaign_id: UUID
    account_id: UUID


class CampaignWithBlueprints(Campaign):
    blueprint: bool
    blueprintComments: list[str]


class Contractor(Owner):
    contractor_id: UUID
    account_id: UUID


class ContractorWithBlueprints(Contractor):
    blueprint: bool
    blueprintComments: list[str]


class Product(Poster):
    product_id: UUID
    campaign_id: UUID
    time_frame_start: str
    time_frame_deadline: str


class ProductWithBlueprints(Product):
    blueprint: bool
    blueprintComments: list[str]


class Resource(Poster):
    resource_id: UUID
    contractor_id: UUID
    ads_format: int
    contractor_type: int
    subs: int
    views: int
    male_gender_ratio: int
    dominant_age: int
    country_id: int
    region_id: int


class ResourceWithBlueprints(Resource):
    blueprint: bool
    blueprintComments: list[str]
