from pydantic import BaseModel


class BlueprintConfirmation(BaseModel):
    blueprint: bool | None = None


class BlueprintComments(BaseModel):
    blueprintComments: list[str] | None = None


class BlueprintComment(BaseModel):
    blueprintComment: str
