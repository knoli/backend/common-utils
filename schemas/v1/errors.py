from fastapi import HTTPException, status
from pydantic import BaseModel


class ErrorResponse(BaseModel):
    message: str = ""
    # code: int = 500


class DBAPICallError(HTTPException):
    def __init__(self, msg: str = "..."):
        super().__init__(
            detail=f"DB api call failed: {msg}",
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
        )


class AuthError(HTTPException):
    def __init__(self, detail, status_code):
        super().__init__(
            detail=detail,
            status_code=status_code,
        )


class TokenNotFound(AuthError):
    def __init__(self):
        super().__init__(
            detail="Token was not found",
            status_code=status.HTTP_401_UNAUTHORIZED,
        )


class TokenIncorrect(AuthError):
    def __init__(self):
        super().__init__(
            detail="Token incorrect",
            status_code=status.HTTP_403_FORBIDDEN,
        )


class WrongAccountId(AuthError):
    def __init__(self):
        super().__init__(
            detail="This product/resource belongs to another author",
            status_code=status.HTTP_403_FORBIDDEN,
        )
