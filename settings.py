import os
from functools import lru_cache

from dotenv import load_dotenv
from redis.asyncio import Redis, from_url
from sqlalchemy.ext.asyncio import async_sessionmaker, create_async_engine

load_dotenv()

CLIENT_URL = "http://knoli.devyaterikov.com"

HOST = os.environ.get("HOST")
PORT = int(os.environ.get("PORT"))
INTERNAL_PORT = int(os.environ.get("INTERNAL_PORT"))
ENV = os.environ.get("ENV")  # "dev" or "prod"
ALLOW_ORIGIN = "http://localhost" if ENV == "dev" else CLIENT_URL
DEFAULT_COOKIE_SETTINGS = {"httponly": True}
DATE_FORMAT = "%Y-%m-%dT%H:%M:%S"

DB_DRIVER = "postgresql+asyncpg"
DB_USER = os.environ.get("DB_USER")
DB_PASS = os.environ.get("DB_PASS")
DB_URL = os.environ.get("DB_URL")
DB_NAME = os.environ.get("DB_NAME")
TEST_DB_NAME = os.environ.get("TEST_DB_NAME")
REDIS_URL = "redis://localhost:6379"

DB_FULL_URL = f"{DB_DRIVER}://{DB_USER}:{DB_PASS}@{DB_URL}/{DB_NAME}"
TEST_DB_FULL_URL = f"{DB_DRIVER}://{DB_USER}:{DB_PASS}@{DB_URL}/{TEST_DB_NAME}"
REDIS_FULL_URL = f"{REDIS_URL}?decode_responses=True"

PROXY_HOST = os.environ.get("PROXY_HOST")
SERVICE_NAMES = os.environ.get("SERVICE_NAMES")
INTERNAL_URLS = {  # Use INTERNAL_URLS[version][service_name]
    "v1": {"__prefix__": f"http://{PROXY_HOST}:{INTERNAL_PORT}/internal/v1"}
}
for ver, _urls_obj in INTERNAL_URLS.items():
    for service_name in SERVICE_NAMES.split(","):
        INTERNAL_URLS[ver][service_name] = INTERNAL_URLS[ver]["__prefix__"] + f"/{service_name}"

CHECK_TOKEN_URL = f"{INTERNAL_URLS['v1']['passport']}/check_token"


@lru_cache
def get_db_sessionmaker() -> async_sessionmaker:
    return async_sessionmaker(
        create_async_engine(DB_FULL_URL),
        expire_on_commit=False,
        autoflush=False,
        autocommit=False,
    )


@lru_cache
def get_redis_sessionmaker() -> Redis:
    return from_url(REDIS_FULL_URL)
