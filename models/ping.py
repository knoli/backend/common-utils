from uuid import UUID, uuid4

from sqlalchemy.orm import Mapped, mapped_column

from models.base import BaseModel


class PingInfoModel(BaseModel):
    __tablename__ = "ping_info"
    id: Mapped[UUID] = mapped_column(primary_key=True, unique=True, default=uuid4, index=True)

    name: Mapped[str] = mapped_column(nullable=False)
