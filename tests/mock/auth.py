from typing import Annotated

from fastapi import Cookie, Depends, HTTPException, WebSocket, WebSocketException, status

from schemas.v1.errors import TokenIncorrect, TokenNotFound
from shared.utils.auth import cookie_apikey


def mock_authenticate(account_id: Annotated[str, Depends(cookie_apikey)]):
    if account_id == "incorrect":
        raise TokenIncorrect()

    elif account_id is None:
        raise TokenNotFound()

    return account_id


async def mock_ws_authenticate(
    _: WebSocket, authorization: Annotated[str | None, Cookie()] = None
) -> str:
    try:
        mock_authenticate(account_id=authorization)
    except HTTPException as e:
        raise WebSocketException(code=status.WS_1008_POLICY_VIOLATION) from e

    return authorization
