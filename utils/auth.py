from typing import Annotated

from fastapi import Cookie, Depends, HTTPException, WebSocket, WebSocketException, status
from fastapi.security import APIKeyCookie
from httpx import AsyncClient

from shared.schemas.v1.errors import TokenIncorrect, TokenNotFound, WrongAccountId
from shared.settings import CHECK_TOKEN_URL

AUTH_COOKIE_KEY = "authorization"

cookie_apikey = APIKeyCookie(name=AUTH_COOKIE_KEY, auto_error=False)


async def authenticate(access_token: Annotated[str, Depends(cookie_apikey)]) -> str:
    if access_token is None:
        raise TokenNotFound()

    async with AsyncClient() as client:
        response = await client.get(
            url=CHECK_TOKEN_URL,
            headers={AUTH_COOKIE_KEY: access_token},
        )

    if response.status_code == status.HTTP_200_OK:
        return response.json()["account_id"]

    elif response.status_code == status.HTTP_403_FORBIDDEN:
        raise TokenIncorrect()


async def ws_authenticate(
    _: WebSocket, authorization: Annotated[str | None, Cookie()] = None
) -> str:
    try:
        return await authenticate(authorization)
    except HTTPException as e:
        raise WebSocketException(code=status.WS_1008_POLICY_VIOLATION) from e


def check_belonging(account_id: str, author_id: str):
    if account_id != author_id:
        raise WrongAccountId()
