from typing import Literal

OwnerType = Literal["campaign", "contractor"]
PosterType = Literal["product", "resource"]
AdObjectType = Literal["campaign", "contractor", "product", "resource"]

order_state_list = [
    "request",
    "conversation",
    "payment",
    "work",
    "completed",
    "done",
    "support_check",
    "cancelled",
]

OrderState = Literal[
    "request",
    "conversation",
    "payment",
    "work",
    "completed",
    "done",
    "support_check",
    "cancelled",
]
