import csv
import json
import re
from datetime import datetime
from uuid import UUID

from models import BaseModel

# from settings import DATE_FORMAT


# expected files in json or csv format
def load_static_file(file_path: str):
    path_prefix = "./tests/static/"
    with open(path_prefix + file_path, "r") as data_file:
        if file_path.endswith(".json"):
            file_content = json.load(data_file)
        elif file_path.endswith(".csv"):
            reader = csv.DictReader(data_file, quoting=csv.QUOTE_NONNUMERIC)
            file_content = list(reader)
        else:
            raise Exception(
                "Invalid filetype for file: " + file_path + ". Must end with .json or .csv"
            )
    return file_content


def prepare_database_object(model: BaseModel, data: dict):
    uuid_regex = re.compile("^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$")
    date_regex = re.compile("^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}$")
    return model(
        **{
            k: UUID(v)
            if type(v) is str and uuid_regex.match(v)
            else (datetime.fromisoformat(v) if type(v) is str and date_regex.match(v) else v)
            for k, v in data.items()
        }
    )


def get_static_database_objects(file_name: str, model: BaseModel):
    data_list = load_static_file(file_name)
    model_objects = [prepare_database_object(model, data) for data in data_list]
    return model_objects
