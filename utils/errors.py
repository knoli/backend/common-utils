import traceback

from fastapi import HTTPException
from fastapi.exceptions import RequestValidationError
from fastapi.responses import Response
from starlette import status

from shared.schemas.v1.errors import ErrorResponse
from shared.settings import ENV


def error_response(message: str, status_code: int) -> Response:
    return Response(
        content=ErrorResponse(message=message).model_dump_json(), status_code=status_code
    )


def get_exc_msg(short_msg: str, exc: Exception):
    if ENV == "dev":
        return (
            str(short_msg)
            + "\n"
            + "".join(traceback.TracebackException.from_exception(exc=exc).format())
        )
    else:
        return short_msg


def __access_exception_handler(exc: HTTPException) -> Response:
    # Logger.http_exception(exc)
    return error_response(message=get_exc_msg(exc.detail, exc), status_code=exc.status_code)


def __validation_exception_handler(exc: RequestValidationError) -> Response:
    # Logger.exception(exc)
    return error_response(
        message="Bad Request: " + get_exc_msg(str(exc), exc),
        status_code=status.HTTP_400_BAD_REQUEST,
    )


def __exception_handler(exc: Exception) -> Response:
    # Logger.exception(exc)
    return error_response(
        message="Internal Server Error: " + get_exc_msg(str(exc), exc),
        status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
    )
