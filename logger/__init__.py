import logging
import traceback

from fastapi import HTTPException

logging.basicConfig(
    level=logging.INFO,
    filename=".logs/passport_log.log",
    filemode="w",
    format="%(asctime)s %(levelname)s %(message)s",
)


class Logger:
    @staticmethod
    def info(msg):
        logging.info(msg=msg)

    @staticmethod
    def http_exception(exc: HTTPException):
        logging.exception(
            msg=f"Failed with HTTPException({exc.status_code}), "
            + f"short msg: {exc.detail}, stack trace:\n"
            + "".join(traceback.TracebackException.from_exception(exc).format())
        )

    @staticmethod
    def exception(exc: Exception):
        logging.exception(
            msg=f"Failed with Exception, short msg: {str(exc)}, stack trace:\n"
            + "".join(traceback.TracebackException.from_exception(exc=exc).format())
        )
