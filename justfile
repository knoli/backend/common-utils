set allow-duplicate-recipes

HELP_PATH := "./just/just.help.txt"
GIT_SCRIPTS_PATH := "../global/git"
SERVICES_SUBS_LIST := ""
SUPPORT_SUBS_LIST := ""

import './just/env.just'
import? '../global/just/env.just'
import? '../global/just/subs.just'


default:
    @just help


_prepare *args:
    #!/bin/bash
    set -euo pipefail

    just mpull
    just _setup {{ args }}


_unsupported_operation:
    #!/bin/bash
    set -euo pipefail
    just _error "Unsupported operation"


prepare *args:
    #!/bin/bash
    set -euo pipefail

    docker login registry.gitlab.com
    just _prepare {{ args }}
    just setup_hooks


run:
    @just _unsupported_operation


test *args:
    @just _unsupported_operation


setup_db:
    @just _unsupported_operation


new_revision rev_name:
    @just _unsupported_operation


alembic *args:
    @just _unsupported_operation
